package ic.compute.string


import ic.compute.Computable
import ic.compute.type.StringType


data class Concat (

	val operandsList : Computable

) : Computable {


	override val type get() = StringType


	override val inputNamesInUse get() = operandsList.inputNamesInUse


	override fun optimize() : Computable {
		return Concat(
			operandsList = operandsList.optimize()
		)
	}


	override fun toString() = "Concat($operandsList)"


}