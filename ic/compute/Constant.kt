package ic.compute


import ic.base.reflect.ext.className
import ic.base.throwables.NotSupportedException
import ic.struct.set.finite.FiniteSet
import ic.util.code.clike.quote.doubleQuote

import ic.math.numbers.Number
import ic.math.numbers.real.integer.Integer

import ic.compute.type.BooleanType
import ic.compute.type.NullType
import ic.compute.type.NumberType
import ic.compute.type.StringType


data class Constant (val value: Any?) : Computable {


	override val type get() = when (value) {
		null -> NullType
		is Boolean -> BooleanType
		is Number -> NumberType
		is String -> StringType
		else -> throw NotSupportedException.Runtime(
			message = "value.className: ${ value.className }"
		)
	}


	override val inputNamesInUse get() = FiniteSet<String>()


	override fun optimize() = this


	override fun toString() = when (value) {
		null -> "N"
		is Boolean -> if (value) "T" else "F"
		is Number -> value.toString()
		is String -> doubleQuote(value)
		else -> throw NotSupportedException.Runtime(
			message = "value.className: ${ value.className }"
		)
	}


	companion object {

		val Null = Constant(null)

		val False = Constant(false)
		val True  = Constant(true)

		val Zero = Constant(Integer.Zero)
		val One  = Constant(Integer.One)
		
	}

	
}