package ic.compute


import ic.struct.set.finite.FiniteSet

import ic.compute.type.Type


interface Computable {


	val type : Type


	val inputNamesInUse : FiniteSet<String>


	fun optimize() : Computable


}