package ic.compute.struct


import ic.struct.collection.Collection

import ic.compute.Computable
import ic.compute.type.CollectionType
import ic.struct.collection.ext.copy.copyConvert
import ic.struct.collection.ext.copy.copyConvertToList
import ic.struct.collection.ext.copy.copyUnion
import ic.struct.list.ext.concat
import ic.struct.list.fromarray.ListFromArray


data class CollectionConstant (

	val items : Collection<Computable>

) : Computable {


	override val type get() = CollectionType


	override val inputNamesInUse get() = items.copyConvert { it.inputNamesInUse }.copyUnion()


	override fun optimize() : Computable {
		return CollectionConstant(
			items = items.copyConvert { it.optimize() }
		)
	}


	override fun toString() = "C(${ items.copyConvertToList { it.toString() }.concat(separator = ',') })"


	constructor (vararg items: Computable) : this (items = ListFromArray(items))


}