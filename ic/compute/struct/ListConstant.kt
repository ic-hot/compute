package ic.compute.struct


import ic.struct.list.fromarray.ListFromArray
import ic.struct.list.List

import ic.compute.Computable
import ic.compute.type.ListType
import ic.struct.list.ext.concat
import ic.struct.list.ext.copy.convert.copyConvert
import ic.struct.list.ext.copy.copyUnion


data class ListConstant (

	val items : List<Computable>

) : Computable {


	override val type get() = ListType


	override val inputNamesInUse get() = items.copyConvert { it.inputNamesInUse }.copyUnion()


	override fun optimize() : Computable {
		return ListConstant(
			items = items.copyConvert { it.optimize() }
		)
	}


	override fun toString() = "L(${ items.copyConvert { it.toString() }.concat(separator = ',') })"


	constructor (vararg items: Computable) : this (items = ListFromArray(items))


}