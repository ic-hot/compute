package ic.compute


import ic.struct.set.finite.FiniteSet

import ic.compute.type.Type


class Input (

	val name : String,

	override val type : Type

) : Computable {


	override fun hashCode() = name.hashCode()

	override fun equals (other: Any?) : Boolean {
		if (other is Input) {
			return this.name == other.name
		} else {
			return false
		}
	}


	override val inputNamesInUse get() = FiniteSet(name)


	override fun optimize() = this


	override fun toString() = name


}