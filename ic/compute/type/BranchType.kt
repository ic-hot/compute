package ic.compute.type


import ic.compute.Computable


data class BranchType (

	val condition : Computable,

	val ifTrue  : Type,
	val ifFalse : Type

) : Type