package ic.compute.bool.branch


import ic.base.throwables.WrongValueException
import ic.struct.set.finite.ext.plus

import ic.compute.Computable
import ic.compute.Constant
import ic.compute.type.BranchType


abstract class Branch : Computable {


	abstract val condition : Computable

	abstract val ifTrue  : Computable
	abstract val ifFalse : Computable


	override val type get() = BranchType(
		condition = condition,
		ifTrue = ifTrue.type,
		ifFalse = ifFalse.type
	)


	override val inputNamesInUse get() = (
		condition.inputNamesInUse +
		ifTrue.inputNamesInUse +
		ifFalse.inputNamesInUse
	)


	override fun optimize() : Computable {
		val optimizedCondition = this.condition.optimize()
		if (optimizedCondition is Constant) {
			when (optimizedCondition.value) {
				true -> return ifTrue.optimize()
				false -> return ifFalse.optimize()
				else -> throw WrongValueException.Runtime(
					message = "optimizedCondition.value: ${ optimizedCondition.value }"
				)
			}
		} else {
			return Branch(
				condition = optimizedCondition,
				ifTrue = ifTrue.optimize(),
				ifFalse = ifFalse.optimize()
			)
		}
	}


	override fun toString() = "?($condition,$ifTrue,$ifFalse)"


}