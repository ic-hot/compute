package ic.compute.bool.branch


import ic.compute.Computable


@Suppress("NOTHING_TO_INLINE")
inline fun Branch (
	condition : Computable,
	ifTrue  : Computable,
	ifFalse : Computable
) : Branch {
	return object : Branch() {
		override val condition get() = condition
		override val ifTrue    get() = ifTrue
		override val ifFalse   get() = ifFalse
	}
}


inline fun Branch (
	condition : Computable,
	crossinline getIfTrue  : () -> Computable,
	crossinline getIfFalse : () -> Computable
) : Branch {
	return object : Branch() {
		override val condition get() = condition
		override val ifTrue    get() = getIfTrue()
		override val ifFalse   get() = getIfFalse()
	}
}