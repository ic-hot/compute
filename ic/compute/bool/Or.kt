package ic.compute.bool


import ic.compute.Computable
import ic.compute.type.BooleanType


data class Or (

	val operandsCollection : Computable

) : Computable {


	override val type get() = BooleanType


	override val inputNamesInUse get() = operandsCollection.inputNamesInUse


	override fun optimize() : Computable {
		return Or(
			operandsCollection = operandsCollection.optimize()
		)
	}


	override fun toString() = "|($operandsCollection)"


}