package ic.compute.bool


import ic.struct.set.finite.ext.plus

import ic.compute.Computable
import ic.compute.Constant
import ic.compute.type.BooleanType


data class Equals (

	val a : Computable,
	val b : Computable

) : Computable {


	override val type get() = BooleanType


	override val inputNamesInUse get() = a.inputNamesInUse + b.inputNamesInUse


	override fun optimize() : Computable {
		val optimizedA = a.optimize()
		val optimizedB = b.optimize()
		if (optimizedA == optimizedB) return Constant(true)
		if (optimizedA is Constant && optimizedB is Constant) return Constant.False
		return Equals(
			a = optimizedA,
			b = optimizedB
		)
	}


	override fun toString() = "=($a,$b)"


}