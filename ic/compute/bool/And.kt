package ic.compute.bool


import ic.compute.Computable
import ic.compute.struct.CollectionConstant
import ic.compute.type.BooleanType


data class And (

	val operandsCollection : Computable

) : Computable {


	override val type get() = BooleanType


	override val inputNamesInUse get() = operandsCollection.inputNamesInUse


	override fun optimize() : Computable {
		val optimizedOperandsCollection = operandsCollection.optimize()
		return And(
			operandsCollection = optimizedOperandsCollection
		)
	}


	override fun toString() = "&($operandsCollection)"


	constructor (vararg operands: Computable) : this (CollectionConstant(*operands))


}