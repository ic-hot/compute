package ic.compute.bool



import ic.math.numbers.real.RealNumber

import ic.compute.Computable
import ic.compute.Constant
import ic.compute.type.NumberType
import ic.struct.set.finite.ext.plus


data class Compare (

	val lesser  : Computable,
	val greater : Computable

) : Computable {


	override val type get() = NumberType


	override val inputNamesInUse get() = lesser.inputNamesInUse + greater.inputNamesInUse


	override fun optimize() : Computable {
		val optimizedLesser = lesser.optimize()
		val optimizedGreater = greater.optimize()
		if (optimizedLesser is Constant && optimizedGreater is Constant) {
			val lesserValue = optimizedLesser.value
			val greaterValue = optimizedGreater.value
			val result : Boolean = (
				if (lesserValue is RealNumber && greaterValue is RealNumber) {
					lesserValue < greaterValue
				} else {
					false
				}
			)
			return Constant(result)
		} else {
			return Compare(
				lesser = optimizedLesser,
				greater = optimizedGreater
			)
		}
	}


	override fun toString() = "<($lesser,$greater)"


}