package ic.compute.bool


import ic.base.throwables.WrongTypeException
import ic.compute.Computable
import ic.compute.Constant
import ic.compute.type.BooleanType


data class Not (

	val operand : Computable

) : Computable {


	override val type get() = BooleanType


	override val inputNamesInUse get() = operand.inputNamesInUse


	override fun optimize() : Computable {
		val optimizedOperand = operand.optimize()
		if (optimizedOperand is Constant) {
			val operandValue = optimizedOperand.value
			if (operandValue is Boolean) {
				return Constant(!operandValue)
			} else {
				throw WrongTypeException.Runtime("operandValue: $operandValue")
			}
		} else {
			return Not(operand = optimizedOperand)
		}
	}


	override fun toString() = "!($operand)"


}