package ic.compute.number


import ic.base.objects.ext.orSkip
import ic.struct.collection.ext.plus

import ic.math.numbers.Number
import ic.math.numbers.funs.product

import ic.compute.Computable
import ic.compute.Constant
import ic.compute.struct.CollectionConstant
import ic.compute.type.NumberType
import ic.struct.collection.ext.copy.copyConvert
import ic.struct.collection.ext.copy.copyFilter
import ic.struct.collection.ext.count.count


data class Product (

	val operandsCollection : Computable

) : Computable {


	override val type get() = NumberType


	override val inputNamesInUse get() = operandsCollection.inputNamesInUse


	override fun optimize() : Computable {
		val optimizedOperands = operandsCollection.optimize()
		if (optimizedOperands !is CollectionConstant) {
			return Product(operandsCollection = optimizedOperands)
		}
		val constantOperands = optimizedOperands.items.copyConvert { (it as? Constant).orSkip }
		if (constantOperands.count < 2) return Product(operandsCollection = optimizedOperands)
		val constantProduct = product(
			constantOperands.copyConvert { (it.value as? Number).orSkip }
		)
		if (constantOperands.count == optimizedOperands.items.count) {
			return Constant(constantProduct)
		}
		return Product(
			operandsCollection = CollectionConstant(
				items = (
					optimizedOperands.items.copyFilter { it !is Constant } +
					Constant(constantProduct)
				)
			)
		)
	}


	override fun toString() = "*($operandsCollection)"


}