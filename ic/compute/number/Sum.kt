package ic.compute.number


import ic.base.objects.ext.orSkip
import ic.compute.Computable
import ic.compute.Constant
import ic.compute.struct.CollectionConstant
import ic.compute.type.NumberType
import ic.math.numbers.Number
import ic.math.numbers.funs.sum
import ic.struct.collection.ext.copy.copyConvert
import ic.struct.collection.ext.copy.copyFilter
import ic.struct.collection.ext.count.count
import ic.struct.collection.ext.plus


data class Sum (

	val operandsCollection : Computable

) : Computable {


	override val type get() = NumberType


	override val inputNamesInUse get() = operandsCollection.inputNamesInUse


	override fun optimize() : Computable {
		val optimizedOperands = operandsCollection.optimize()
		if (optimizedOperands !is CollectionConstant) {
			return Sum(operandsCollection = optimizedOperands)
		}
		val constantOperands = optimizedOperands.items.copyConvert { (it as? Constant).orSkip }
		if (constantOperands.count < 2) return Sum(operandsCollection = optimizedOperands)
		val constantSum = sum(constantOperands.copyConvert { (it.value as? Number).orSkip })
		if (constantOperands.count == optimizedOperands.items.count) return Constant(constantSum)
		return Sum(
			operandsCollection = CollectionConstant(
				items = (
					optimizedOperands.items.copyFilter { it !is Constant } +
					Constant(constantSum)
				)
			)
		)
	}


	override fun toString() = "+($operandsCollection)"


}