package ic.compute.number


import ic.math.numbers.Number
import ic.math.numbers.ext.unaryMinus

import ic.compute.Computable
import ic.compute.Constant
import ic.compute.type.NumberType


data class Negate (

	val operand : Computable

) : Computable {


	override val type get() = NumberType


	override val inputNamesInUse get() = operand.inputNamesInUse


	override fun optimize() : Computable {
		val optimizedOperand = operand.optimize()
		if (optimizedOperand is Constant) {
			val value = optimizedOperand.value as Number
			return Constant(
				value = -value
			)
		} else {
			return Negate(
				operand = optimizedOperand
			)
		}
	}


	override fun toString() = "-($operand)"


}